import FontFaceObserver from 'fontfaceobserver';

(function() {
	var regular = new FontFaceObserver('Urban Grotesk'),
		semibold = new FontFaceObserver('Urban Grotesk', { wieght: 600 });

	Promise.all([regular.load(), semibold.load()]).then(function () {
		document.documentElement.className += ' has-fonts-loaded';
	});

	// Optimization for Repeat Views
	sessionStorage.foutFontsLoaded = true;
})();
