import debounce from 'lodash/debounce';
import * as helpers from './helpers';

const initMainNav = () => {
	function init() {
		const $nav = $('.main-nav');

		if ( $nav.length ) {
			const $body = $('body');
			const $topBar = $nav.closest('.page-header-top-bar');
			let hamburger = false;

			if ( 'matchMedia' in window ) {
				const bp = 'only screen and (min-width: 56.5em)'; // when changing the breakpoint, update it in `_media.scss`

				// mobile
				if ( ! matchMedia(bp).matches ) {
					makeHamburger($nav, $topBar, $body);
					hamburger = true;
				}

				// reset/initialize mobile behaviour on window resize
				$(window).on('resize', debounce(function() {
					const matches = matchMedia(bp).matches;

					// desktop -> mobile
					if ( ! matches && ! hamburger ) {
						makeHamburger($nav, $topBar, $body);
						hamburger = true;
					}
					// mobile -> desktop
					else if ( matches && hamburger ) {
						destroyHamburger($nav, $topBar, $body);
						hamburger = false;
					}
				}, 150));
			}
		}
	}

	function makeHamburger( $nav, $topBar, $body ) {
		const label = $nav.find('#main-nav-label').text().trim();
		let $toggle = $(
			`<button class="main-nav-toggle" type="button" aria-controls="main-nav" aria-expanded="false" title="${label}">
				<span class="main-nav-toggle-hamburger">
					<span class="main-nav-toggle-hamburger-line"></span>
					<span class="main-nav-toggle-hamburger-line"></span>
					<span class="main-nav-toggle-hamburger-line"></span>
				</span>
				<span class="main-nav-toggle-label">${label}</span>
			</button>`
		);
		const $mainNavLink = $('.main-nav-link');

		$mainNavLink.click(() => {
			$toggle.trigger('click');
		})

		// if focus is in nav, move it to button
		let moveFocus = false;
		if ( $(document.activeElement).closest('.main-nav').length ) {
			moveFocus = true;
		}

		$nav
			.attr('aria-hidden', 'true')
			.addClass('is-collapsible');

		$topBar.addClass('has-main-nav-collapsible');

		$toggle
			.on('click', function() {
				if ( $toggle.attr('aria-expanded') == 'false' ) {
					$toggle.attr('aria-expanded', 'true');
					$nav.attr('aria-hidden', 'false');
					$body.addClass('has-main-nav-in-overlay');
					tabCycle( $topBar );
					$topBar.css({
						'height': '100%',
						'height': '100vh'
					});
					$topBar.addClass('is-open');
				}
				else {
					$toggle.attr('aria-expanded', 'false');
					$nav.attr('aria-hidden', 'true');
					tabCycleOff( $topBar );
					$body.removeClass('has-main-nav-in-overlay');
					$topBar.css({
						'height': 'auto',
						'height': 'auto'
					});
					$topBar.removeClass('is-open');
				}
			})
			.appendTo( $topBar.find('.page-header-top-bar-without-nav') );

		helpers.updatePageHeaderTopBarOffset();

		if ( moveFocus ) {
			$toggle.focus();
		}
	}

	function destroyHamburger( $nav, $topBar, $body ) {
		const $toggle = $topBar.find('.main-nav-toggle');

		$nav
			.attr('aria-hidden', 'false')
			.removeClass('is-collapsible');

		$body.removeClass('has-main-nav-in-overlay');
		$topBar.removeClass('has-main-nav-collapsible');
		$topBar.css({
			'height': 'auto',
			'height': 'auto'
		});

		tabCycleOff( $topBar );

		// if focus is on toggle, move it to nav
		if ( $toggle.get(0) === document.activeElement ) {
			$nav.find('.main-nav-link').first().focus();
		}

		$toggle.remove();
	}

	/*
	 * Cycle focus (pressing tab) inside `$element`.
	 */
	function tabCycle( $element ) {
		var focusableSelector = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, [tabindex="0"], [contenteditable]'; // all focusable elements (https://classroom.udacity.com/courses/ud891/lessons/7962031279/concepts/79621414230923)

		$element.on('keydown', function(e) {
			if ( e.keyCode === 9 ) { // Tab
				// DOM could be updated, check new elements every time
				var $focusable = $(focusableSelector, $element).filter(':not([tabindex="-1"])');

				// last -> first
				if ( e.target == $focusable.last().get(0) && ! e.shiftKey ) {
					e.preventDefault();
					$focusable.first().focus();
				}
				// first -> last
				else if ( e.target == $focusable.first().get(0) && e.shiftKey ) {
					e.preventDefault();
					$focusable.last().focus();
				}
			}
		});
	}

	/*
	 * Remove the tab cycle from element.
	 */
	function tabCycleOff( $element ) {
		$element.off('keydown');
	}

	init();
};

export default initMainNav;
