export const updatePageHeaderTopBarOffset = () => {

	const $wpBar = $('#wpadminbar');

	if ( $wpBar.length && $('#wpadminbar').css('position') == 'fixed' ) {
		$('.page-header-top-bar').css('top', $wpBar.outerHeight());
	}
};
