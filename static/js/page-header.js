import throttle from 'lodash/throttle';
import * as helpers from './helpers';

const initPageHeader = () => {
	function init() {
		const $topBar = $('.page-header-top-bar');
		const $topBarHome = $('.page-header-top-bar.is-home');

		if ( $topBar.length && 'matchMedia' in window ) { // require `matchMedia` to allow closed tab cycle inside opened hamburger menu
			const bp = 'screen only and (min-width: 56.5em)';  // when changing the breaikpoint, update it in `_media.scss`

			helpers.updatePageHeaderTopBarOffset();

			// reset/initialize mobile behaviour on window resize
			$(window).on('resize', throttle(function() {
				helpers.updatePageHeaderTopBarOffset();
			}, 150));

			$(window).on('scroll', function() {
		    if($(window).scrollTop() > 100) {
	        $topBarHome.removeClass('is-top');
		    } else {
					$topBarHome.addClass('is-top');
		    }
			});
		}
	}

	init();
};

export default initPageHeader;
