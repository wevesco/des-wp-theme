import '../scss/main.scss';
import '../img/graphics/logo-white.svg';
import '../img/graphics/logo-minimal-white.svg';
import '../img/graphics/scroll-arrow.svg';
import '../img/graphics/arrow-right.svg';
import '../img/graphics/logo-colored.svg';

import initPlugins from './plugins.js';
import initPageHeader from './page-header';
import initMainNav from './main-nav';
import initContactMap from './contact-map';

initPlugins();

window.initGoogleMap = initContactMap;

$(function() {
    // animated scroll for anchonr links
	$('a[href^="#"]').on('click', function(e) {
		e.preventDefault();
		var anchor = $(this).attr('href');
		$('html,body').animate({
			scrollTop: $(anchor).offset().top - 100, // navbar height
		}, 250);
	});

	initPageHeader();
	initMainNav();
});
