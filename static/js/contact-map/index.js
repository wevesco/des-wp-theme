import styles from './styles.js';

const initContactMap = () => {
	const $map = $('.contact-map');

	if ( $map.length ) {
		const position = { lat: 50.0816461, lng: 14.4151872 }; // Drn, Národní 135/14
		const map = new google.maps.Map($map.get(0), {
			center: position,
			zoom: 15,
			styles,
			zoomControl: true,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			rotateControl: false,
			fullscreenControl: false,
		});

		new google.maps.Marker({ position, map });
	}
};

export default initContactMap;
