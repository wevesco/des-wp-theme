<?php

/**
 * Clean WordPress header (for cleaning emoji and oEmbed is used a plugin)
 */
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'wp_generator' );

/**
 * Enqueue styles and scripts
 */
add_action( 'wp_enqueue_scripts', function() {

	/* Load styles and add a cache-breaking URL parameter */

	$fileName = '/assets/main.css';
	$fileUrl = get_template_directory_uri() . $fileName;
	$filePath = get_template_directory() . $fileName;
	wp_enqueue_style( 'main', $fileUrl, [], filemtime($filePath), 'all' );

	/* Load scripts and add a cache-breaking URL parameter */

	$fileName = '/assets/vendor.js';
	$fileUrl = get_template_directory_uri() . $fileName;
	$filePath = get_template_directory() . $fileName;
	wp_enqueue_script( 'vendor', $fileUrl, [], filemtime($filePath), true );

	$fileName = '/assets/main.js';
	$fileUrl = get_template_directory_uri() . $fileName;
	$filePath = get_template_directory() . $fileName;
	wp_enqueue_script( 'main', $fileUrl, ['vendor'], filemtime($filePath), true );

} );
