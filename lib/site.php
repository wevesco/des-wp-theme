<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	return;
}

Timber::$dirname = [ 'templates', 'views' ];

class NanoEnergiesSite extends TimberSite {

	function __construct() {
		add_theme_support( 'menus' );
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'get_twig', [ $this, 'add_to_twig' ] );
		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['header_menu'] = new Timber\Menu( 'header-menu' );
		$context['site'] = $this;
		$context['static_assets_url'] = $this->get_static_assets_url();
		$context['footer_menu_1'] = new Timber\Menu('footer-menu-1');
		$context['footer_menu_2'] = new Timber\Menu('footer-menu-2');
		$context['footer_menu_3'] = new Timber\Menu('footer-menu-3');

		// Pass only if WPML plugin is activated
		if ( function_exists( 'icl_get_languages' ) ) {
    		$context['languages'] = icl_get_languages();
		}
		return $context;
	}

	function add_to_twig( $twig ) {
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( 'static_assets', new Twig_SimpleFilter( 'static_assets', [ $this, 'static_assets' ] ) );
		$twig->addFilter( 'date_i18n', new Twig_SimpleFilter( 'date_i18n', [ $this, 'date_i18n' ] ) );
		return $twig;
	}

	function static_assets( $filePath ) {
	  return $this->get_static_assets_url() . $filePath;
	}

	function get_static_assets_url() {
		return $this->theme->link . '/assets/';
	}

	function date_i18n( $date ) {
		$date_format = get_option( 'date_format' );
		$timestamp = strtotime( $date );
	  return date_i18n( $date_format, $timestamp );
	}

}

new NanoEnergiesSite();
