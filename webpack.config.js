var BundleTracker = require('webpack-bundle-tracker');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var FaviconsWebpackPlugin = require('favicons-webpack-plugin');
var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var cssMqPacker = require('css-mqpacker');

var resolve = path.resolve.bind(path, __dirname);

var extractTextPlugin;
var fileLoaderPath;
var output;

output = {
	path: resolve('assets/'),
	filename: '[name].js',
};
fileLoaderPath = 'file-loader?name=[name].[ext]';
extractTextPlugin = new ExtractTextPlugin('[name].css');

var bundleTrackerPlugin = new BundleTracker({
	filename: 'webpack-bundle.json',
});

var commonsChunkPlugin = new webpack.optimize.CommonsChunkPlugin({
	names: 'vendor',
	chunks: [
		'main',
	],
});

var occurenceOrderPlugin = new webpack.optimize.OccurrenceOrderPlugin();

var environmentPlugin = new webpack.DefinePlugin({
	'process.env': {
		NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
	},
});

var providePlugin = new webpack.ProvidePlugin({
	$: 'jquery',
	jQuery: 'jquery',
	'window.jQuery': 'jquery',
});

var faviconsWebpackPlugin = new FaviconsWebpackPlugin({
	logo: './static/img/favicon.svg',
	prefix: 'favicons/',
	title: 'DES',
});

var config = {
	entry: {
		main: './static/js/main.js',
		vendor: [
			'jquery',
			'svg-injector-2',
		],
		fonts: [
			'./static/js/fonts.js'
		],
	},
	output: output,
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
			},
			{
				test: /\.scss$/,
				loader: ExtractTextPlugin.extract({
					use: [
						{
							loader: 'css-loader',
							options: {
								'sourceMap': true,
							}
						},
						{
							loader: 'postcss-loader',
							options: {
								'sourceMap': true,
								'plugins': function () {
									return [
										autoprefixer,
										cssMqPacker({ sort: true })
									];
								},
							},
						},
						{
							loader: 'sass-loader',
							options: {
								'sourceMap': true,
							},
						},
					],
				}),
			},
			{
				test: /\.(eot|otf|png|svg|jpg|ttf|woff|woff2|json)(\?v=[0-9.]+)?$/,
				use: [
					{
						loader: fileLoaderPath,
					},
					{
						loader: 'image-webpack-loader',
					},
				],
				include: [
					resolve('node_modules'),
					resolve('static/font'),
					resolve('static/js'),
					resolve('static/img'),
				],
			},
		],
	},
	plugins: [
		bundleTrackerPlugin,
		commonsChunkPlugin,
		environmentPlugin,
		extractTextPlugin,
		occurenceOrderPlugin,
		providePlugin,
		faviconsWebpackPlugin,
	],
	resolve: {
		alias: {
			'jquery': resolve('node_modules/jquery/dist/jquery.js'),
		},
	},
};

module.exports = config;
